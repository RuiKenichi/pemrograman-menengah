package Minggu1;

public class Prog_01 {
    public static void main(String[] args) {
        System.out.println("==========================================");
        System.out.println("||           BIODATA MAHASISWA           ||");
        System.out.println("==========================================");
        System.out.println("|| Nama         : Rizky Juniardi         ||");
        System.out.println("|| NPM          : 2211010005             ||");
        System.out.println("|| Program Studi: S1 Teknik Informatika  ||");
        System.out.println("|| Mata Kuliah  : Pemrograman Menengah   ||");
        System.out.println("==========================================");
        System.out.println("||       Created By: Rizky Juniardi      ||");
        System.out.println("==========================================");
    }
}
