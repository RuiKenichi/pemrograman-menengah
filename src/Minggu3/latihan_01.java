package Minggu3;

import java.util.Scanner;

public class latihan_01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan nilai akhir mata kuliahnya: ");
        int nilai = input.nextInt();

        if (nilai < 55) {
            System.out.println("Mahasiswa tersebut tidak lulus");
        } else {
            System.out.println("Mahasiswa tersebut lulus");
        }
    }
}
