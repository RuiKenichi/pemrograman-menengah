package Minggu3;

import java.util.Scanner;

public class latihan_03 {
    public static void main(String[] args) {
        // tampilkan menu hitung luas, keliling, panjang diagonal, dan keluar
        System.out.println("Menu PERSEGI PANJANG");
        System.out.println("====================");
        System.out.println("1. Hitung Luas");
        System.out.println("2. Hitung Keliling");
        System.out.println("3. Hitung Panjang Diagonal");
        System.out.println("4. Keluar");
        System.out.print("Pilih menu (1/2/3/4): ");

        // ambil inputan dari user
        Scanner input = new Scanner(System.in);
        int pilihan = input.nextInt();


        switch (pilihan) {
            case 1 -> {
                System.out.println("\nHitung Luas");
                // inisialisasi variabel hitung luas persegi panjang
                int panjang, lebar, luas;
                // ambil inputan dari user
                System.out.print("Masukkan panjang: ");
                panjang = input.nextInt();
                System.out.print("Masukkan lebar: ");
                lebar = input.nextInt();
                // hitung luas
                luas = panjang * lebar;
                // tampilkan hasil
                System.out.println("Luas persegi panjang adalah " + luas);
            }
            case 2 -> {
                System.out.println("\nHitung Keliling");
                // inisiaisasi variabel hitung keliling persegi panjang
                int panjang, lebar, keliling;
                // ambil inputan dari user
                System.out.print("Masukkan panjang: ");
                panjang = input.nextInt();
                System.out.print("Masukkan lebar: ");
                lebar = input.nextInt();
                // hitung keliling
                keliling = 2 * (panjang + lebar);
                // tampilkan hasil
                System.out.println("Keliling persegi panjang adalah " + keliling);
            }
            case 3 -> {
                System.out.println("\nHitung Panjang Diagonal");
                // inisialisasi variabel hitung panjang diagonal persegi panjang
                int panjang, lebar, diagonal;
                // ambil inputan dari user
                System.out.print("Masukkan panjang: ");
                panjang = input.nextInt();
                System.out.print("Masukkan lebar: ");
                lebar = input.nextInt();
                // hitung panjang diagonal
                diagonal = (int) Math.sqrt(Math.pow(panjang, 2) + Math.pow(lebar, 2));
                // tampilkan hasil
                System.out.println("Panjang diagonal persegi panjang adalah " + diagonal);
            }
            case 4 -> {
                System.out.println("Keluar");
                // keluar dari program dengan error code 0
                System.exit(0);
            }
            default -> {
                System.out.println("Pilihan tidak tersedia");
                // keluar dari program dengan error code 1
                System.exit(1);
            }
        }

    }
}
