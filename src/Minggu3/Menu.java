package Minggu3;

public class Menu {
    public static void main(String[] args) {
        int menu = 4;

        switch (menu) {
            case 1 -> {
                System.out.println("Kamu memilih menu - Ayam Penyet");
                System.out.println("Harga: Rp. 20.000");
            }
            case 2 -> {
                System.out.println("Kamu memilih menu - Pepes Kakap");
                System.out.println("Harga: Rp. 30.000");
            }
            case 3 -> {
                System.out.println("Kamu memilih menu - Rendang");
                System.out.println("Harga: Rp. 25.000");
            }
            case 4 -> {
                System.out.println("Kamu memilih menu - Telur Dadar");
                System.out.println("Harga: Rp. 10.000");
            }
            default -> System.out.println("Maaf, menu tidak tersedia");
        }
    }
}