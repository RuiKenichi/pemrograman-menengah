package Minggu3;

import java.util.Scanner;

public class latihan_02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan nilai mata kuliahnya: ");
        int nilai = input.nextInt();
        if (nilai >= 80)
            System.out.println("Nilai A");
        else if (nilai >= 70)
            System.out.println("Nilai B");
        else if (nilai >= 55)
            System.out.println("Nilai C");
        else if (nilai >= 40)
            System.out.println("Nilai D");
        else
            System.out.println("Nilai E");
    }
}
