package Minggu6.dosenMahasiswa;

public class Main {
    public static void main(String[] args) {
        // membuat objek mahasiswa 1
        Mahasiswa mhs1 = new Mahasiswa();
        mhs1.setNama("Rizki Juniardi");
        mhs1.setNim("2211010005");

        // membuat objek mahasiswa 2
        Mahasiswa mhs2 = new Mahasiswa();
        mhs2.setNama("Abdul Jalal");
        mhs2.setNim("2211010003");

        // membentuk objek dosen
        Dosen dosen = new Dosen();
        dosen.setKodeDosen("RBD");

        // hubungkan objek mahasiswa dengan objek dosen
        dosen.setNimMahasiswa(mhs1.getNim());
        dosen.setNimMahasiswa(mhs2.getNim());

        // tampilkan data mahasiswa yang diambil dari objek dosen
        System.out.println("Kode Dosen: " + dosen.getKodeDosen());
        System.out.println("Dosen Mengajar Mahasiswa: ");

        // ambil jumlah mahasiswa yang diambil dari objek dosen
        int jumlahMahasiswa = dosen.getJumlahMahasiswa();

        // ambil nim mahasiswa yang diambil dari objek dosen dengan perulangan
        for (int i = 0; i < jumlahMahasiswa; i++) {
            System.out.println("\nNIM: " + dosen.getNimMahasiswa(i));
            switch (i) {
                case 0 -> System.out.println("Nama: " + mhs1.getNama());
                case 1 -> System.out.println("Nama: " + mhs2.getNama());
            }
        }
    }
}
