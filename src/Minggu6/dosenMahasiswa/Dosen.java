package Minggu6.dosenMahasiswa;

public class Dosen {
    private String kodeDosen;
    private final String[] nimMahasiswa = new String[5];
    private int jumlahMahasiswa;

    public void setKodeDosen(String kodeDosen) {
        this.kodeDosen = kodeDosen;
    }

    public void setNimMahasiswa(String nimMahasiswa) {
        if (jumlahMahasiswa < this.nimMahasiswa.length) {
            this.nimMahasiswa[jumlahMahasiswa] = nimMahasiswa;
            jumlahMahasiswa++;
        }
    }

    public String getKodeDosen() {
        return this.kodeDosen;
    }

    public int getJumlahMahasiswa() {
        return this.jumlahMahasiswa;
    }

    public String getNimMahasiswa(int index) {
        return (nimMahasiswa[index]);
    }
}
