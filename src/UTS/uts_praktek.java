package UTS;

import java.util.Scanner;

public class uts_praktek {

    public static void main(String[] args) {
        // Buat scanner object
        Scanner input = new Scanner(System.in);

        // Minta pengguna untuk masukin golongan dari karyawan
        System.out.print("Masukkan golongan karyawan (1, 2, atau 3): ");
        int golongan = input.nextInt();

        // Deklarasikan variabel untuk gaji pokok, potongan covid 19, dan THR
        int gajiPokok = 0;
        double potonganCovid = 0;
        double thr = 0;

        // Tetapkan nilai ke variabel berdasarkan golongan
        switch (golongan) {
            case 1 -> {
                gajiPokok = 300000;
                potonganCovid = 0;
                thr = 0.5 * gajiPokok;
            }
            case 2 -> {
                gajiPokok = 500000;
                potonganCovid = 0.05 * gajiPokok;
                thr = 0.5 * gajiPokok;
            }
            case 3 -> {
                gajiPokok = 750000;
                potonganCovid = 0.15 * gajiPokok;
                thr = gajiPokok;
            }
            default -> {
                System.out.println("Golongan tidak valid.");
                System.exit(0);
            }
        }

        // tampilkam gaji pokok, potongan covid 19 dalam persentase, dan THR dalam persentase
        System.out.println("==================================================");
        System.out.println("Golongan: " + golongan);
        System.out.println("==================================================");
        System.out.println("Gaji pokok: Rp " + gajiPokok);
        System.out.println("Potongan covid 19: " + (int) (potonganCovid / gajiPokok * 100) + "%");
        System.out.println("Tunjangan hari raya: " + (int) (thr / gajiPokok * 100) + "%");

        // hitung total gaji
        double totalGaji = gajiPokok - potonganCovid + thr;

        // tampilkan hasil ke layar
        System.out.println("==================================================");
        System.out.println("Total gaji: Rp " + totalGaji);
        System.out.println("==================================================");

    }
}