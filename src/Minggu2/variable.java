package Minggu2;

public class variable {
    public static void main(String[] args) {
        // Deklarasi variabel
        int i;
        long x;
        double y;
        float c;
        // pengisian nilai
        i = 12423;
        x = 738;
        y = 14.576;
        c = 34.123f;
        // mencetak nilai
        System.out.println("Nilai i adalah: " + i);
        System.out.println("Nilai x adalah: " + x);
        System.out.println("Nilai y adalah: " + y);
        System.out.println("Nilai c adalah: " + c);
    }
}