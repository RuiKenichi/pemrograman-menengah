package Minggu2;

public class keliling_luas {
    public static void main(String[] args) {
        // inisialisasi variabel luas persegi panjang
        int panjang = 10;
        int lebar = 5;
        int luas = panjang * lebar;
        // menampilkan hasil luas persegi panjang
        System.out.println("Luas persegi panjang adalah: " + luas);

        // inisialisasi variabel keliling dan luas lingkaran
        int jari_jari = 7;
        double phi = 3.14;
        double keliling = 2 * phi * jari_jari;
        double luas_lingkaran = phi * jari_jari * jari_jari;
        // menampilkan hasil keliling dan luas lingkaran
        System.out.println("Keliling lingkaran adalah: " + keliling);
        System.out.println("Luas lingkaran adalah: " + luas_lingkaran);

        // inisialisasi variabel luas segitiga
        int alas = 15;
        int tinggi = 10;
        double luas_segitiga = 0.5 * alas * tinggi;
        // menampilkan hasil luas segitiga
        System.out.println("Luas segitiga adalah: " + luas_segitiga);
    }
}
