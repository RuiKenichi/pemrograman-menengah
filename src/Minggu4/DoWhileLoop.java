package Minggu4;

public class DoWhileLoop {
    public static void main(String[] args) {
        // do while loop
        int i = 0;
        do {
            System.out.println(i);
            i++;
        } while (i < 10);
    }
}
