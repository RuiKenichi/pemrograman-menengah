package Minggu4;

import java.util.Scanner;

public class BilanganPrima {
    public static void main(String[] args) {
        // panggil method untuk menginput batas awal dan batas akhir
        inputNumber();
    }

    // method untuk menginput batas awal dan batas akhir
    public static void inputNumber() {
        // deklarasi variabel
        int batasAwal, batasAkhir;
        Scanner input = new Scanner(System.in);

        System.out.println("Deret Bilangan Prima");
        // Dapatkan batas awal dan batas akhir dari user
        System.out.print("Masukkan batas awal: ");
        batasAwal = input.nextInt();
        System.out.print("Masukkan batas akhir: ");
        batasAkhir = input.nextInt();
        System.out.println("---------------------------------------");

        // print bilangan prima diantara batas awal dan batas akhir
        showPrimeNumber(batasAwal, batasAkhir);
    }

    // method untuk menampilkan bilangan prima
    public static void showPrimeNumber(int batasAwal, int batasAkhir) {
        // print bilangan prima diantara batas awal dan batas akhir
        System.out.println("Bilangan prima antara " + batasAwal + " dan " + batasAkhir + " adalah: ");
        // loop untuk mengecek setiap bilangan diantara batas awal dan batas akhir
        for (int i = batasAwal; i <= batasAkhir; i++) {
            if (isPrime(i)) {
                System.out.print(i + " ");
            }
        }
    }

    // Check jika sebuah bilangan adalah bilangan prima
    public static boolean isPrime(int number) {
        // pengecekan bilangan prima harus lebih besar dari 1
        if (number <= 1) {
            return false;
        }
        // loop untuk mengecek setiap bilangan diantara 2 dan akar kuadrat dari bilangan tersebut
        for (int i = 2; i <= Math.sqrt(number); i++) {
            // jika bilangan tersebut habis dibagi dengan bilangan lain, maka bukan bilangan prima
            if (number % i == 0) {
                return false;
            }
        }
        // jika tidak habis dibagi dengan bilangan lain, maka bilangan tersebut adalah bilangan prima
        return true;
    }
}