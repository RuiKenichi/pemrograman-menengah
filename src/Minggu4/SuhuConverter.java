package Minggu4;

import java.util.Scanner;

public class SuhuConverter {
    public static void main(String[] args) {
        // inisialisasi variabel celcius dan fahrenheit
        double celcius, fahrenheit;

        // tentukan nilai maksimum celcius yang akan dikonversi dengan di inputkan
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan bnayaknya data: ");
        int maxCelcius = input.nextInt();


        // Print Header Tabel
        System.out.println("No.  Celsius  Fahrenheit");
        System.out.println("------------------------");

        // konversi celcius ke fahrenheit dan cetak ke tabel
        for (int i = 1; i <= maxCelcius; i++) {
            celcius = i;
            fahrenheit = (9.0 / 5) * celcius + 32;
            System.out.printf("%2d.    %3.0f      %3.1f\n", i, celcius, fahrenheit);
        }
    }
}