package Minggu4;

public class Lanjut {
    public static void main(String[] args) {
        // Cetak Angka Ganjil Saja
        System.out.println("Cetak Angka Ganjil Saja");
        for (int i = 0; i < 20; i++) {
            if (i % 2 == 0) {
                continue;
            }
            System.out.println(i);
        }

        // Cetak Angka Genap Saja
        System.out.println("\n\nCetak Angka Genap Saja");
        for (int i = 0; i < 20; i++) {
            if (i % 2 != 0) {
                continue;
            }
            System.out.println(i);
        }
    }
}
