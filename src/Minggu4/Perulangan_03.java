package Minggu4;

import java.util.Scanner;

public class Perulangan_03 {
    public static void main(String[] args) {
        // perulangan untuk mengitung nilai yang diinput dari keyboard menggunakan For Loop
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan batas bilangannya: ");

        int batas = input.nextInt();
        int hasil = 0;
        for (int i = 0; i <= batas; i++) {
            hasil += i;
        }
        System.out.println("Total jumlahnya adalah: " + hasil);
    }
}
