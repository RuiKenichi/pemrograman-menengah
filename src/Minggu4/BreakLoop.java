package Minggu4;

public class BreakLoop {
    public static void main(String[] args) {
        // Array Sayuran
        String[] sayuran = {"Bayam", "Kangkung", "Tomat", "Mentimun", "Tomat", "Selada", "Brokoli"};

        // Cari sayuran yang bernama "Tomat"
        System.out.println("Cari array sayuran yang bernama \"Tomat\"");

        // for loop
        for (String sayur : sayuran) {
            System.out.println(sayur);

            if (sayur.equals("Tomat")) {
                System.out.println("Ditemukan sayuran bernama \"Tomat\"");
                break;
            }
        }
    }
}
