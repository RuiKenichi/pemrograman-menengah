package Minggu4;

import java.util.Scanner;

public class Perulangan_02 {
    public static void main(String[] args) {
        // peruangan untuk menghitung nilai faktorial yang diinputkan oleh user menggunakan do while
        int N, i, jumlah;
        jumlah = 0;
        i = 1;

        // input nilai N
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan nilai N: ");
        N = input.nextInt();

        // hitung nilai faktorial
        do {
            jumlah = jumlah + i;
            i++;
        } while (i <= N);

        // cetak hasil
        System.out.println("Jumlah deret " + N + " adalah = " + jumlah);
    }
}
