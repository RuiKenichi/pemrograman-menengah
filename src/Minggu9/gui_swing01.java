package Minggu9;

import javax.swing.*;
import java.awt.*;

public class gui_swing01 {
    private final JFrame f;
    private double a, b, c, hasil;

    public gui_swing01() {
        f = new JFrame("Kalkulator");
    }

    public void hitungBilangan() {
        JTabbedPane tab = new JTabbedPane();
        tab.add(createPenjumlahanPanel(), "Penjumlahan Bilangan");
        tab.add(createPenguranganPanel(), "Pengurangan Bilangan");
        tab.add(createPerkalianPanel(), "Perkalian Bilangan");
        tab.add(createPembagianPanel(), "Pembagian Bilangan");

        f.getContentPane().add(tab, BorderLayout.NORTH);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(600, 300);
        f.setVisible(true);
    }

    private JPanel createPenjumlahanPanel() {
        JPanel panel = new JPanel();
        JLabel label1 = new JLabel("Bilangan 1:");
        JLabel label2 = new JLabel("Bilangan 2:");
        JTextField input1 = new JTextField(10);
        JTextField input2 = new JTextField(10);
        JButton button = new JButton("Penjumlahan");
        panel.add(label1);
        panel.add(input1);
        panel.add(label2);
        panel.add(input2);
        panel.add(button);

        button.addActionListener(e -> {
            try {
                a = Double.parseDouble(input1.getText());
                b = Double.parseDouble(input2.getText());
                c = a + b;
                String konv = "Hasil Penjumlahan adalah ";
                JOptionPane.showConfirmDialog(null, konv + c, "Hasil",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception j) {
                JOptionPane.showConfirmDialog(null, "Masukkan Angka!", "Error",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
            }
        });

        return panel;
    }

    private JPanel createPenguranganPanel() {
        JPanel panel = new JPanel();
        JLabel label1 = new JLabel("Bilangan 1:");
        JLabel label2 = new JLabel("Bilangan 2:");
        JTextField input1 = new JTextField(10);
        JTextField input2 = new JTextField(10);
        JButton button = new JButton("Pengurangan");
        panel.add(label1);
        panel.add(input1);
        panel.add(label2);
        panel.add(input2);
        panel.add(button);

        button.addActionListener(e -> {
            try {
                a = Double.parseDouble(input1.getText());
                b = Double.parseDouble(input2.getText());
                c = a - b;
                String konv = "Hasil Pengurangan adalah ";
                JOptionPane.showConfirmDialog(null, konv + c, "Hasil",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception j) {
                JOptionPane.showConfirmDialog(null, "Masukkan Angka!", "Error",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
            }
        });

        return panel;
    }

    private JPanel createPerkalianPanel() {
        JPanel panel = new JPanel();
        JLabel label1 = new JLabel("Bilangan 1:");
        JLabel label2 = new JLabel("Bilangan 2:");
        JTextField input1 = new JTextField(10);
        JTextField input2 = new JTextField(10);
        JButton button = new JButton("Perkalian");
        panel.add(label1);
        panel.add(input1);
        panel.add(label2);
        panel.add(input2);
        panel.add(button);

        button.addActionListener(e -> {
            try {
                a = Double.parseDouble(input1.getText());
                b = Double.parseDouble(input2.getText());
                hasil = a * b;
                String konv = "Hasil Perkalian adalah ";
                JOptionPane.showConfirmDialog(null, konv + hasil, "Hasil",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception j) {
                JOptionPane.showConfirmDialog(null, "Masukkan Angka!", "Error",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
            }
        });

        return panel;
    }

    private JPanel createPembagianPanel() {
        JPanel panel = new JPanel();
        JLabel label1 = new JLabel("Bilangan 1:");
        JLabel label2 = new JLabel("Bilangan 2:");
        JTextField input1 = new JTextField(10);
        JTextField input2 = new JTextField(10);
        JButton button = new JButton("Pembagian");
        panel.add(label1);
        panel.add(input1);
        panel.add(label2);
        panel.add(input2);
        panel.add(button);

        button.addActionListener(e -> {
            try {
                a = Double.parseDouble(input1.getText());
                b = Double.parseDouble(input2.getText());
                if (b != 0) {
                    hasil = a / b;
                    String konv = "Hasil Pembagian adalah ";
                    JOptionPane.showConfirmDialog(null, konv + hasil, "Hasil",
                            JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showConfirmDialog(null, "Pembagian dengan 0 tidak valid!", "Error",
                            JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception j) {
                JOptionPane.showConfirmDialog(null, "Masukkan Angka!", "Error",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
            }
        });

        return panel;
    }

    public static void main(String[] args) {
        gui_swing01 kalkulator = new gui_swing01();
        kalkulator.hitungBilangan();
    }
}
