package Minggu9;
import javax.swing.*;

public class swing_01 {
        public static void main(String[] args) {
            String Panjang;
            String Lebar;
            int l,p;
            float hasil;
            // Meminta Input dari User hanya memiliki 1 parameter
            Panjang = JOptionPane.showInputDialog("Panjang");
            Lebar = JOptionPane.showInputDialog("Lebar ");
            //Fungsi untuk merubah dari string menjadi Integer
            l = Integer.parseInt(Lebar);
            p = Integer.parseInt(Panjang);
            hasil = p * l;
            JOptionPane.showMessageDialog(null, "Luas Persegi Panjang adalah "
                    + hasil, "Results", JOptionPane.PLAIN_MESSAGE);
            System.exit(0);
        }
}
