package Minggu9;

public class LatihanString {
    public static void main(String[] args) {
        String str1, str2, gabung ;
        str1 = "Selamat Belajar ";
        str2 = "Program Java";
        gabung = str1.concat(str2);

        String upperCaseText = gabung.toUpperCase();
        String lowerCaseText = gabung.toLowerCase();
        System.out.println("Diberi String : "+str1);
        System.out.println("Diberi String : "+str2);
        System.out.println("String di gabung : "+gabung);
        System.out.println("Text di uppercase: " + upperCaseText);
        System.out.println("Text di lowercase: " + lowerCaseText);
        System.out.println("Panjang String1 Adalah : "+str1.length());
        System.out.println("Panjang String2 Adalah : "+str2.length());
    }
}
