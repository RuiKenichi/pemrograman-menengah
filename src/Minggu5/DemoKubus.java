package Minggu5;

// Class Kotak
class Kotak {
    double panjang;
    double lebar;
    double tinggi;
}


public class DemoKubus {
    public static void main(String[] args) {
        // Deklarasi variabel
        double volume;
        Kotak k = new Kotak();

        // mengisikan nilai ke dalam data-data kelas Kotak
        k.panjang = 7;
        k.lebar = 5;
        k.tinggi = 6;

        // menghitung volume kubus
        volume = k.panjang * k.lebar * k.tinggi;

        // menampilkan hasil
        System.out.println("Volume kubus adalah = " + volume);
    }
}
