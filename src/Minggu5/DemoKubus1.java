package Minggu5;

class Kotak1 {
    double panjang;
    double lebar;
    double tinggi;
}

public class DemoKubus1 {
    public static void main(String[] args) {
        // deklarasi variabel
        double volume1, volume2;
        Kotak1 k1 = new Kotak1();
        Kotak1 k2 = new Kotak1();

        // mengisikan nilai ke dalam objek k1
        k1.panjang = 7;
        k1.lebar = 5;
        k1.tinggi = 6;

        // mengisikan nilai ke dalam objek k2
        k2.panjang = 7;
        k2.lebar = 5;
        k2.tinggi = 56;

        // menghitung volume kubus dari objek k1
        volume1 = k1.panjang * k1.lebar * k1.tinggi;

        // menghitung volume kubus dari objek k2
        volume2 = k2.panjang * k2.lebar * k2.tinggi;

        // menampilkan hasil volume kubus dari objek k1 dan k2
        System.out.println("Volume kubus k1 adalah = " + volume1);
        System.out.println("Volume kubus k2 adalah = " + volume2);
    }
}
