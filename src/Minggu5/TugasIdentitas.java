package Minggu5;

class Identitas {
    String nama;
    String nim;
    String jurusan;
    String tempatLahir;
    String tanggalLahir;

    public Identitas(String nama, String nim, String jurusan, String tempatLahir, String tanggalLahir) {
        this.nama = nama;
        this.nim = nim;
        this.jurusan = jurusan;
        this.tempatLahir = tempatLahir;
        this.tanggalLahir = tanggalLahir;
    }

    public void tampil() {
        System.out.println("Nama : " + nama);
        System.out.println("NIM : " + nim);
        System.out.println("Jurusan : " + jurusan);
        System.out.println("Tempat/ Tgl Lahir : " + tempatLahir + ", " + tanggalLahir);
    }
}

class Ktm extends Identitas {
    String prodi;

    public Ktm(String nama, String nim, String jurusan, String tempatLahir, String tanggalLahir, String prodi) {
        super(nama, nim, jurusan, tempatLahir, tanggalLahir);
        this.prodi = prodi;
    }

    public void tampil() {
        super.tampil();
        System.out.println("Program Studi : " + prodi);
    }
}

public class TugasIdentitas {
    public static void main(String[] args) {
        Ktm mahasiswa = new Ktm("Rizky Juniardi", "2211010005", "Ilmu Komputer", "Bandar Lampung", "16-06-2003", "Teknik Informatika");
        mahasiswa.tampil();
    }
}
